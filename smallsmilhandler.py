#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        # lista de atributos
        self.atributos = {
            "root-layout": ["width", "height", "background-color"],
            "region": ["id", "top", "bottom", "left", "right"],
            "img": ["src", "region", "begin", "dur"],
            "audio": ["src", "begin", "dur"],
            "textstream": ["src", "region"]
            }
        self.lista = []

    def startElement(self, nombre, attrs):
        if nombre in self.atributos:
            dicc = {}
            dicc["etiqueta"] = nombre
            for names in self.atributos[nombre]:
                dicc[names] = attrs.get(names, "")
            self.lista.append(dicc)

    def get_tags(self):
        return self.lista


if __name__ == "__main__":
    parser = make_parser()
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open('karaoke.smil'))
    lista = sHandler.get_tags()
