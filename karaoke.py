#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from smallsmilhandler import SmallSMILHandler
from xml.sax import make_parser
import json
from urllib.request import urlretrieve


class KaraokeLocal():
    # inicializamos
    def __init__(self, file):
        try:
            parser = make_parser()
            sHandler = SmallSMILHandler()
            parser.setContentHandler(sHandler)
            parser.parse(open(file))
            self.listas = sHandler.get_tags()  # Creación de la lista
        except FileNotFoundError:
            sys.exit("file not found")  # Caso en el que el archivo no exista

    def __str__(self):
        salida = ""
        for frase in self.listas:
            word = frase["etiqueta"]
            salida += word
            for atrib in frase:
                if atrib != "etiqueta" and frase[atrib]:
                    salida += "\t" + atrib + "=" + '"' + frase[atrib] + '"'
                    # Suma los atributos hasta llegar al salto de linea
            salida += "\n"  # Salto de linea
        return(salida)  # Nos devuelve los valores en línea

    def do_local(self):
        for frase in self.listas:
            for etiqueta in frase:
                if etiqueta == "src" and "http://" in frase["src"]:
                    download = frase["src"].split("/")[-1]
                    urlretrieve(frase["src"], download)
                    frase["src"] = download
    # Buscamos la etiqueta src y la dividimos por las barras,
    # quedándonos con el áltimo elemento que será el que decarguemos
    # de manera local

    def to_json(self, file, filejson=""):
        if not filejson:
            filejson = (file.split(".")[0] + ".json")
        with open(filejson, "w") as outfile:
            json.dump(self.listas, outfile, indent=4)
    # Carga la lista en el archivo.json
    # Lo indentamos


if __name__ == "__main__":
    file = (sys.argv[1])
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 karaoke.py file.smil.")
    karaoke = KaraokeLocal(file)
    print(karaoke)
    karaoke.to_json(file, "")
    karaoke.do_local()
    karaoke.to_json(file, 'local.json')
    print(karaoke)
